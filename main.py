#!/usr/bin/env python

import os
import urllib
import urllib2
import json

from google.appengine.api import users

import jinja2
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)
	
class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write('Hello world!')
        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render())

class SubHandler(webapp2.RequestHandler):
    def get(self):
        sub = self.request.get("sub")
        response = urllib2.urlopen( "https://www.reddit.com/r/" + sub + ".json" ).read()
	data = json.loads(response)
        stories = []
        for story in data["data"]["children"]:
            stories.append( [ story["data"]["id"], story["data"]["title"], story["data"]["num_comments"], story["data"]["url"] ] )
        for n in stories:
            print(n[0])
        template_values = { 'stories': stories, 'sub': sub}
        template = JINJA_ENVIRONMENT.get_template('subreddit.html')
        self.response.write(template.render(template_values))

class ComHandler(webapp2.RequestHandler):
    def get(self):
        #https://www.reddit.com/r/worldnews/comments/2vnbwm.json
        response = urllib2.urlopen( "https://www.reddit.com/r/" + self.request.get("sub") + "/comments/" + self.request.get("story") + ".json" ).read()
        data = json.loads(response)
        comments = []
        for y in data:
            for x in y["data"]["children"]:
                try:
                    comments.append([ x["data"]["body"], x["data"]["author"] ])
                except:
                    print "er"
        template_values = { 'comments': comments }
        template = JINJA_ENVIRONMENT.get_template('comments.html')
        self.response.write(template.render(template_values))

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/topic', SubHandler),
    ('/comments', ComHandler)
], debug=True)
